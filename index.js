const express = require('express');
const mongoose = require('mongoose');
const dotenv = require('dotenv');
const app = express();
const authRoute = require('./routes/auth') //import routes
const cartRoute = require('./routes/cart');

dotenv.config();


//connect to db
mongoose.connect(process.env.DB_CONNECT, 
{useNewUrlParser: true, useUnifiedTopology: true},
()=>
console.log('connected to db'));

app.use(express.json()) //middleware 


//route middlewares
app.use('/backend', authRoute); 
app.use('/cart', cartRoute);


//server listen
app.listen(3000, ()=>{
    console.log('server works!')
})